import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ParametroEspectral {
	final static File ARQUIVO1 = new File("C:\\Users\\dell\\Desktop\\Arnaldo\\EAMM.xls");
	final static File ARQUIVO2 = new File("C:\\Users\\dell\\Desktop\\Arnaldo\\EAPM.xls");
	final static File ARQTRABALHO = new File("C:\\Users\\stf698547\\Desktop\\CC.xls");

	// 1E18
	final static Long MAIOR_RAIOX = 1000000000000000000L;
	// 1E16
	final static Long MAIOR_ULTRAVIOLETA = 10000000000000000L;
	// 1E14
	final static Long MAIOR_INFRAVERMELHO = 100000000000000L;
	// 1E12
	final static Long MENOR_INFRAVERMELHO = 1000000000000L;
	// 1E8
	final static Long MENOR_MICROONDAS = 100000000L;

	public static void main(String[] args) throws BiffException, IOException {
		NumberFormat formatter = new DecimalFormat("0.##E0");
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe Faixa Espectral: ");
		String faixaEspectral = sc.nextLine();
		faixaEspectral = faixaEspectral.replace("-", "");
		System.out.println(faixaEspectral);
		escolherFaixa(faixaEspectral.toLowerCase(), formatter);
	}

	private static void escolherFaixa(String faixaEspectral, NumberFormat formatter) throws BiffException, IOException {
		switch (faixaEspectral) {
		case "infravermelho":
			imprimirInfraVermelhos(formatter);
			break;
		case "microondas":
			imprimirMicroondas(formatter);
			break;
		case "ultravioleta":
			imprimirUltraVioleta(formatter);
			break;
		case "raiox":
			imprimirRaioX(formatter);
			break;

		default:
			System.out.println("Faixa Espectral nao implementada");
			System.out.println("Adeus!!");
			break;
		}
	}

	private static void imprimirRaioX(NumberFormat formatter) throws BiffException, IOException {
		String[][] matriz = lerExcel(ARQTRABALHO);
		int count = 0;
		for (int i = 1; i < matriz.length; i++) {
			String aux = matriz[i][1];
			aux = aux.replace(".", "");
			Long valorCorrente = Double.valueOf(aux).longValue();
			if (MAIOR_RAIOX > valorCorrente && valorCorrente > MAIOR_ULTRAVIOLETA) {
				System.out.println("Ponto : " + matriz[i][0] + "| " + formatter.format(valorCorrente));
				count++;
			}
		}
		System.out.println("Quantidade de Evidencias: " + count);

	}

	private static void imprimirUltraVioleta(NumberFormat formatter) throws BiffException, IOException {
		String[][] matriz = lerExcel(ARQTRABALHO);
		int count = 0;
		for (int i = 1; i < matriz.length; i++) {
			String aux = matriz[i][1];
			aux = aux.replace(".", "");
			Long valorCorrente = Double.valueOf(aux).longValue();
			if (MAIOR_ULTRAVIOLETA > valorCorrente && valorCorrente > MAIOR_INFRAVERMELHO) {
				System.out.println("Ponto : " + matriz[i][0] + "| " + formatter.format(valorCorrente));
				count++;
			}
		}
		System.out.println("Quantidade de Evidencias: " + count);
	}

	private static void imprimirMicroondas(NumberFormat formatter) throws BiffException, IOException {
		String[][] matriz = lerExcel(ARQTRABALHO);
		int count = 0;
		for (int i = 1; i < matriz.length; i++) {
			String aux = matriz[i][1];
			aux = aux.replace(".", "");
			Long valorCorrente = Double.valueOf(aux).longValue();
			if (MENOR_INFRAVERMELHO > valorCorrente && valorCorrente > MENOR_MICROONDAS) {
				System.out.println("Ponto: " + matriz[i][0] + "| " + formatter.format(valorCorrente));
				count++;
			}
		}
		System.out.println("Quantidade de Evidencias: " + count);
	}

	private static void imprimirInfraVermelhos(NumberFormat formatter) throws BiffException, IOException {
		String[][] matriz = lerExcel(ARQTRABALHO);
		int count = 0;
		for (int i = 1; i < matriz.length; i++) {
			String aux = matriz[i][1];
			aux = aux.replace(".", "");
			Long valorCorrente = Double.valueOf(aux).longValue();
			if (MAIOR_INFRAVERMELHO > valorCorrente && valorCorrente > MENOR_INFRAVERMELHO) {
				System.out.println("Ponto: " + matriz[i][0] + "| " + formatter.format(valorCorrente));
				count++;
			}
		}
		System.out.println("Quantidade de Evidencias: " + count);

	}

	private static void calcularSemelhanca(String[][] mat1, String[][] mat2) {
		int achados = 0;
		int total = 0;
		if (mat1.length <= mat2.length) {
			for (int i = 1; i < mat1.length; i++) {
				if (mat1[i][0].equals(mat2[i][0])) {
					achados++;

				}
			}
			total = mat1.length - 1;
		} else {
			for (int i = 1; i < mat2.length; i++) {
				if (mat1[i][0].equals(mat2[i][0])) {
					achados++;
				}
			}
			total = mat2.length - 1;
		}
		double passo1 = total - achados;
		double passo2 = passo1 / total;
		double passo3 = passo2 - 1;

		double passo4 = passo3 * 100;

		if (passo4 < 0) {
			passo4 = passo4 * -1;
		}
		DecimalFormat df = new DecimalFormat("0.##");
		String porcentagemFormatada = df.format(passo4);

		// FIXME CONVERSAR COM NEIVA SOBRE CONTA LOUCA
		double conta = (((total - achados) / total) - 1) * 100;

		System.out.print("Porcentagem de Semelhanca: " + porcentagemFormatada + "%");

	}

	private static String[][] lerExcel(File path) throws IOException, BiffException {
		Workbook workbook = Workbook.getWorkbook(path);
		Sheet sheet = workbook.getSheet(0);
		int qtdLinhas = sheet.getRows();
		int qtdColunas = sheet.getColumns();

		String[][] matrizResultado = new String[qtdLinhas][qtdColunas];
		System.out.println("Iniciando a leitura do Arquivo: " + path.getName() + "...");
		for (int i = 0; i < qtdLinhas; i++) {
			Cell a1 = sheet.getCell(0, i);
			Cell a2 = sheet.getCell(1, i);
			// getContents retorna um valor do tipo String
			matrizResultado[i][0] = a1.getContents();
			matrizResultado[i][1] = a2.getContents();
		}
		workbook.close();
		System.out.println("Leitura Completa!");
		return matrizResultado;
	}
}
