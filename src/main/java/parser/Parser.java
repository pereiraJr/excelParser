package parser;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Parser {
	final static File ARQUIVO1 = new File("C:\\Users\\dell\\Desktop\\Arnaldo\\EAMM.xls");
	final static File ARQUIVO2 = new File("C:\\Users\\dell\\Desktop\\Arnaldo\\EAPM.xls");

	public static void main(String[] args) throws BiffException, IOException {

		String value = "0.1169793009758";
		value = value.replace(".", "");
		System.out.println("STRING SEM PONTO: " + value);
		
		NumberFormat formatter = new DecimalFormat();
		Long maxinteger = Double.valueOf(value).longValue();
		//Long maxinteger = Long.MAX_VALUE;
		System.out.println(maxinteger);
		formatter = new DecimalFormat("0.######E0");
		System.out.println(formatter.format(maxinteger));

		// String[][] matriz1 = lerExcel(ARQUIVO1);
		// String[][] matriz2 = lerExcel(ARQUIVO2);
		// int totallinhas = matriz2.length - 1;
		// calcularSemelhanca(matriz1, matriz2);

	}

	private static void calcularSemelhanca(String[][] mat1, String[][] mat2) {
		int achados = 0;
		int total = 0;
		if (mat1.length <= mat2.length) {
			for (int i = 1; i < mat1.length; i++) {
				if (mat1[i][0].equals(mat2[i][0])) {
					achados++;

				}
			}
			total = mat1.length - 1;
		} else {
			for (int i = 1; i < mat2.length; i++) {
				if (mat1[i][0].equals(mat2[i][0])) {
					achados++;
				}
			}
			total = mat2.length - 1;
		}
		double passo1 = total - achados;
		double passo2 = passo1 / total;
		double passo3 = passo2 - 1;

		double passo4 = passo3 * 100;

		if (passo4 < 0) {
			passo4 = passo4 * -1;
		}
		DecimalFormat df = new DecimalFormat("0.##");
		String porcentagemFormatada = df.format(passo4);

		// FIXME CONVERSAR COM NEIVA SOBRE CONTA LOUCA
		double conta = (((total - achados) / total) - 1) * 100;

		System.out.print("Porcentagem de Semelhanca: " + porcentagemFormatada + "%");

	}

	private static String[][] lerExcel(File path) throws IOException, BiffException {
		Workbook workbook = Workbook.getWorkbook(path);
		Sheet sheet = workbook.getSheet(0);
		int qtdLinhas = sheet.getRows();
		int qtdColunas = sheet.getColumns();

		String[][] matrizResultado = new String[qtdLinhas][qtdColunas];
		System.out.println("Iniciando a leitura do Arquivo: " + path.getName() + "...");
		for (int i = 0; i < qtdLinhas; i++) {
			Cell a1 = sheet.getCell(0, i);
			Cell a2 = sheet.getCell(1, i);
			// getContents retorna um valor do tipo String
			matrizResultado[i][0] = a1.getContents();
			matrizResultado[i][1] = a2.getContents();
		}
		workbook.close();
		System.out.println("Leitura Completa!");
		return matrizResultado;
	}
}
